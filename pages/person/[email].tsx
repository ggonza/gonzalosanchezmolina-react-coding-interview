import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

interface Person {
  name: string;
  gender: string;
  phone: string;
  birthday: string;
}

const PersonDetail = () => {
  const [isEditing, setIsEditing] = useState(false);
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );
  const [candidateData, setCandidateData] = useState<Person>(data);

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const onEditHandler = () => {
    setIsEditing(true);
  };

  const onDataChange = (fieldName) => (event) => {
    const { value } = event.target;
    setCandidateData((state) => ({ ...state, [fieldName]: value }));
  };

  const savePersonData = () => {
    save(candidateData);
    setIsEditing(false);
  };

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={onEditHandler}>
            Edit
          </Button>,
        ]}
      >
        {data && (
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">
              {isEditing ? (
                <Input
                  name="name"
                  onChange={onDataChange('name')}
                  value={candidateData.name}
                />
              ) : (
                data.name
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Gender">
              {isEditing ? (
                <Input
                  name="gender"
                  onChange={onDataChange('gender')}
                  value={candidateData.gender}
                />
              ) : (
                data.gender
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Phone">
              {isEditing ? (
                <Input
                  name="phone"
                  onChange={onDataChange('phone')}
                  value={candidateData.phone}
                />
              ) : (
                data.phone
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Birthday">
              {isEditing ? (
                <Input
                  name="birthday"
                  onChange={onDataChange('birthday')}
                  value={candidateData.birthday}
                />
              ) : (
                data.birthday
              )}
            </Descriptions.Item>
          </Descriptions>
        )}
        {isEditing && (
          <Button type="primary" onClick={savePersonData}>
            Guardar
          </Button>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
